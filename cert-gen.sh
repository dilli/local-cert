#!/bin/bash

# Generate a CA certificate and an end-entity certificate for
# sam.local using minica (https://github.com/jsha/minica) if not
# existing.
#
# This will be executed in the `cert` container, when starting the
# Docker Compose setup.
#
# The ./localhost-cacert.crt can be imported in your browser to trust
# the certificate (in Chrome go to Settings > Privacy and Security >
# Security > Manage certificates, then choose the tab Authorities and
# hit Import)

CERT_FILE=./localhost-cacert.crt
KEY_FILE=./localhost-cakey.key

if [[ ! -f "$CERT_FILE" || ! -d ./_.localhost.local ]]; then
    echo "Generating certificates..."
    rm -rf $CERT_FILE $KEY_FILE ./localhost.local ./_.localhost.local
    minica -ca-cert $CERT_FILE -ca-key $KEY_FILE -domains "*.localhost.local,localhost.local"
    ln -s _.localhost.local localhost.local
else
    echo "Certificates exist already"
fi

