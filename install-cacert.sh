#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

CERT_FILE=./localhost-cacert.crt

cd "$(dirname "${BASH_SOURCE[0]}")"

installed () {
  command -v "$1" &> /dev/null
}

if [ ! -f "$CERT_FILE" ]; then
  echo "The cacert does not exist yet, generating it now..."
  docker-compose up cert
fi

case "$OSTYPE" in
  darwin*)  
    echo "You are running on OSX" 
    echo "Installing cacert to system keychain"
    sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain "$CERT_FILE"
    #sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain "$CERT_FILE" -P

    ;;

  linux*)   
    echo "You are running on LINUX"
    if ! command -v "certutil" &> /dev/null; then
      echo "you must install the package 'libnss3-tools' or install the cacert manually in chrome"
    else
      echo "Installing to the nssdb"
      certutil -d "sql:$HOME/.pki/nssdb" -D -n "https-localhost" 2>/dev/null || true
      certutil -d "sql:$HOME/.pki/nssdb" -A -t "C,," -n "https-localhost" -i "$CERT_FILE"
    fi
    ;;

  msys*)    
    echo "WINDOWS" 
    ;;

  *)
    echo "I have no idea how to install a cacert on $OSTYPE".
    echo "You are on your own, sorry"
    ;;
  
esac